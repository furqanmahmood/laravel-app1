<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    //
    public function index(){
        $data = [
            'title' => 'Welcome to LVAPP1!',
        ];
        return view('pages.index')->with($data);
    }

    public function services(){
        $data = [
            'title' => 'Services we offer',
            'services' => ['Web Design', 'Web Development', 'Digital Marketing', 'SEO', 'Performance Optimization']
        ];
        return view('pages.services')->with($data);
    }

    public function about(){
        $data = [
            'title' => 'About Us'
        ];
        return view('pages.about')->with($data);
    }
}
