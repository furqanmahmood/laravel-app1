@extends('layouts.app-default')

@section('content')
        <h1>{{$title}}</h1>
        <p>We are in the SERVICES page of our APP</p>
        @if(count($services)>0)
            <ul class="list">
                @foreach($services as $service)
                    <li>
                        {{$service}}
                    </li>
                @endforeach
            </ul>
        @endif
@endsection